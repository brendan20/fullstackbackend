import ember
import lightgbm as lgb
from random import random


class CalypsoToyBackend:

    def predict(self, file, file_type, environment, model):
        # check inputs
        self.input_error_checks(file, file_type, environment, model)

        if file_type == 'PE':
            model_file_path = 'data/ember_train.txt'
            lgbm_model = lgb.Booster(model_file=model_file_path)
            score = ember.predict_sample(lgbm_model, file)
        else:
            score = random()

        return {'score': score, 'is_malware': score > 0.5}

    class ReturnOptions:
        @staticmethod
        def environment():
            environment = ['Windows 7 32 bit',
                           'Windows 7 64 bit',
                           'Windows 8 32 bit',
                           'Windows 8 64 bit',
                           'Windows 10 32 bit',
                           'Windows 10 64 bit']
            return environment

        @staticmethod
        def file_type():
            file_type = ['PE',
                         'PDF',
                         'XLS',
                         'DOC',
                         'PPT']
            return file_type

        @staticmethod
        def model():
            models = ['EMBER v1.0',
                      'EMBER v1.1',
                      'EMBER v1.3',
                      'EMBER v2.0']
            return models

    def input_error_checks(self, file, file_type, environment, model):
        # assert that file has been inputted as bytes
        assert isinstance(file, bytes), 'invalid file inputted, expected bytes'

        # assert that a valid file type was provided
        valid_files = self.ReturnOptions.file_type()
        assert valid_files.__contains__(file_type), 'invalid file type inputted'

        # assert that a valid file type was provided
        valid_envs = self.ReturnOptions.environment()
        assert valid_envs.__contains__(environment), 'invalid environment type inputted'

        # assert that a valid file type was provided
        valid_models = self.ReturnOptions.model()
        assert valid_models.__contains__(model), 'invalid model type inputted'
