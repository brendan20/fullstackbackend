import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open(os.path.join(os.path.dirname(__file__), "README.md"), encoding="UTF-8") as f:
    readme = f.read()

version = "0.1.0"
package_data = {}
setup(
    name="calypso_backend",
    version=version,
    description="Endgame Malware BEnchmark for Research",
    long_description=readme,
    package_data=package_data,
    author_email="")
