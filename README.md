This repo contains the code required to build a backend for the fullstack engineering challange.
To get started follow these steps.

1.) clone the repo

    git clone https://gitlab.com/brendan20/fullstackbackend.git
    cd fullstackbackend/

2.) set up and activate virtual enviroment with python 3.6 (replace path as required)

    virtualenv venv -p /Library/Frameworks/Python.framework/Versions/3.6/bin/Python3.6
    . venv/bin/activate

3.) install required packages

    pip install -r requitments.txt
    
4.) If on MacOS you may need to install OpenMP C++ library

    brew install libomp

5.) example.py shows how you can interact with the backend and run sample files

6.) Build an API around this functionality to interact with a DB and frontend